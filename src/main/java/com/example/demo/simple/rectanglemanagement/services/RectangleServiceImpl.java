/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.simple.rectanglemanagement.services;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.simple.rectanglemanagement.model.Rectangle;
import com.example.demo.simple.rectanglemanagement.repositories.RectangleRepository;

import lombok.RequiredArgsConstructor;

/**
 *
 * @author pgsousa
 *
 */
@Service
@RequiredArgsConstructor
public class RectangleServiceImpl implements RectangleService {

	private final RectangleRepository repository;
	private final RectangleCreateMapper rectangleEditMapper;

	@Override
	public Iterable<Rectangle> findAll() {
		return repository.findAll();
	}

	@Override
	public Optional<Rectangle> findOne(final int id) {
		return repository.findById(id);
	}

	@Override
	public Rectangle create(final CreateRectangleRequest resource) {
		// construct a new object based on data received by the service
		final Rectangle obj = rectangleEditMapper.create(resource);
		return repository.save(obj);
	}

	@Override
	public Rectangle update(final int id, final EditRectangleRequest resource) {
		final var rect = repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Cannot update an object that does not yet exist"));

		// update
		rect.updateData(resource.getDescription(), resource.getLength(), resource.getWidth());

		return repository.save(rect);
	}

	@Override
	@Transactional
	public int deleteById(final int id) {
		return repository.deleteById(id);
	}

	@Override
	public Rectangle partialUpdate(final int id, final EditRectangleRequest resource) {
		final var rect = repository.findById(id)
				.orElseThrow(() -> new NotFoundException("Cannot update an object that does not yet exist"));

		// since we got the object from the database we can apply the patch
		rect.applyPatch(resource.getDescription(), resource.getLength(), resource.getWidth());

		// TODO in the meantime some other user might have changed this object on the
		// database, so we should handle concurrency control
		return repository.save(rect);
	}

	@Override
	public Iterable<Rectangle> findByArea(final Long area) {
		return repository.findByArea(area);
	}

	@Override
	public Iterable<Rectangle> findByMinLength(final Integer minLength) {
		return repository.findByLengthGreaterThan(minLength);
	}

}
