/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.foomanagement.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.hibernate.StaleObjectStateException;
import org.junit.jupiter.api.Test;

import com.example.demo.foomanagement.services.EditFooRequest;

/**
 *
 * @author pgsousa
 *
 */
class FooTest {

	@Test
	void ensureNameMustNotBeNull() {
		assertThrows(IllegalArgumentException.class, () -> new Foo(null));
	}

	@Test
	void ensureNameMustNotBeBlank() {
		assertThrows(IllegalArgumentException.class, () -> new Foo(""));
	}

	@Test
	void ensureNameMustNotBeBlankSpaces() {
		assertThrows(IllegalArgumentException.class, () -> new Foo("     "));
	}

	@Test
	void ensureNameIsSet() {
		final var subject = new Foo("name");
		assertEquals("name", subject.getName());
	}

	@Test
	void ensureWidthIsSet() {
		final var subject = new Foo("name");
		subject.setWidth(50);
		assertEquals(Integer.valueOf(50), subject.getWidth().get());
	}

	@Test
	void ensureWidthIsUnSet() {
		final var subject = new Foo("name");
		subject.setWidth(50);
		assertEquals(Integer.valueOf(50), subject.getWidth().get());
		subject.setWidth(null);
		assertFalse(subject.getWidth().isPresent());
	}

	@Test
	void ensureLengthIsSet() {
		final var subject = new Foo("name");
		subject.setLength(50);
		assertEquals(Integer.valueOf(50), subject.getLength().get());
	}

	@Test
	void ensureLengthIsUnSet() {
		final var subject = new Foo("name");
		subject.setLength(50);
		assertEquals(Integer.valueOf(50), subject.getLength().get());
		subject.setLength(null);
		assertFalse(subject.getLength().isPresent());
	}

	@Test
	void whenVersionIsStaledItIsNotPossibleToPatch() {
		final var subject = new Foo("name");

		assertThrows(StaleObjectStateException.class, () -> subject.applyPatch(999, "other", null, null));
	}

	@Test
	void ensurePatchNameIsIgnored() {
		final var patch = new EditFooRequest("other", 1, 2);
		final var subject = new Foo("name");

		subject.applyPatch(subject.getVersion(), patch.getDescription(), patch.getLength(), patch.getWidth());

		assertEquals("name", subject.getName());
	}

	@Test
	void ensurePatchWidth() {
		final var patch = new EditFooRequest("other", null, 50);

		final var subject = new Foo("name");
		subject.setWidth(99);

		subject.applyPatch(0, patch.getDescription(), patch.getLength(), patch.getWidth());

		assertEquals(Integer.valueOf(50), subject.getWidth().get());
	}

	@Test
	void whenWidthIsNotSetPatchingWithValueSets() {
		final var patch = new EditFooRequest("other", null, 50);

		final var subject = new Foo("name");
		assertFalse(subject.getWidth().isPresent());

		subject.applyPatch(0, patch.getDescription(), patch.getLength(), patch.getWidth());

		assertEquals(Integer.valueOf(50), subject.getWidth().get());
	}

	@Test
	void whenWidthIsNotSetPatchingWithNullHasNoEffect() {
		final var patch = new EditFooRequest("other", null, null);
		assertNull(patch.getWidth());

		final var subject = new Foo("name");
		assertFalse(subject.getWidth().isPresent());

		subject.applyPatch(0, patch.getDescription(), patch.getLength(), patch.getWidth());

		assertFalse(subject.getWidth().isPresent());
	}

	@Test
	void ensurePatchLength() {
		final var patch = new EditFooRequest("other", 50, null);

		final var subject = new Foo("name");
		subject.setLength(99);

		subject.applyPatch(0, patch.getDescription(), patch.getLength(), patch.getWidth());

		assertEquals(Integer.valueOf(50), subject.getLength().get());
	}

	@Test
	void whenLengthIsNotSetPatchingWithValueSets() {
		final var patch = new EditFooRequest("other", 50, null);

		final var subject = new Foo("name");
		assertFalse(subject.getLength().isPresent());

		subject.applyPatch(0, patch.getDescription(), patch.getLength(), patch.getWidth());

		assertEquals(Integer.valueOf(50), subject.getLength().get());
	}

	@Test
	void whenLengthIsNotSetPatchingWithNullHasNoEffect() {
		final var patch = new EditFooRequest("other", null, 50);
		assertNull(patch.getLength());

		final var subject = new Foo("name");
		assertFalse(subject.getLength().isPresent());

		subject.applyPatch(0, patch.getDescription(), patch.getLength(), patch.getWidth());

		assertFalse(subject.getLength().isPresent());
	}

	@Test
	void whenLengthAndWidthAreSetAreaIsCalculated() {
		final var subject = new Foo("name");
		subject.setLength(50);
		subject.setWidth(5);
		assertEquals(Long.valueOf(250), subject.getArea().get());
	}

	@Test
	void whenNeitherLengthNorWidthAreSetAreaIsNotCalculated() {
		final var subject = new Foo("name");
		assertFalse(subject.getArea().isPresent());
	}

	@Test
	void whenLengthIsSetButWidthIsNotSetAreaIsNotCalculated() {
		final var subject = new Foo("name");
		subject.setLength(50);
		assertFalse(subject.getArea().isPresent());
	}

	@Test
	void whenLengthIsNotSetButWidthIsSetAreaIsNotCalculated() {
		final var subject = new Foo("name");
		subject.setWidth(50);
		assertFalse(subject.getArea().isPresent());
	}
}
