/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.foomanagement.infrastructure.repositories.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.demo.foomanagement.model.Foo;
import com.example.demo.foomanagement.repositories.FooRepository;

/**
 *
 * @author pgsousa
 *
 */
public interface SpringDataFooRepository extends FooRepository, CrudRepository<Foo, Long> {

	@Override
	Optional<Foo> findByName(String name);

	@Override
	@Query("SELECT f FROM Foo f WHERE f.name LIKE :namepat")
	List<Foo> searchByName(@Param("namepat") String namepat);

	@Override
	List<Foo> findByLengthGreaterThan(int minLength);

	@Override
	@Query("SELECT f FROM Foo f WHERE f.length * f.width >= :area")
	List<Foo> findByArea(@Param("area") int area);

	@Override
	@Modifying
	@Query("DELETE FROM Foo f WHERE f.name = ?1 AND f.version = ?2")
	int deleteByNameIfMatch(String id, long desiredVersion);
}