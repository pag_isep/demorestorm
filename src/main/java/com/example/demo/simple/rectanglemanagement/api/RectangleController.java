/*
 * Copyright (c) 2022-2023 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.simple.rectanglemanagement.api;

import java.net.URISyntaxException;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.simple.rectanglemanagement.model.Rectangle;
import com.example.demo.simple.rectanglemanagement.services.CreateRectangleRequest;
import com.example.demo.simple.rectanglemanagement.services.EditRectangleRequest;
import com.example.demo.simple.rectanglemanagement.services.RectangleService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

/**
 * A simple resource example.
 *
 * @author pgsousa
 *
 */
@Tag(name = "Rectangle", description = "Simple endpoint for managing Rectangles. This is a simple example just to kickstart the use of Spring")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/rectangle")
class RectangleController {

	private final RectangleService service;

	@Operation(summary = "Gets all rectangles", description = "Allows to search rectangle by area or minimum length (not cumulative). If no search criteria is specified all rectangles are returned.")
	@ApiResponse(description = "Success", responseCode = "200", content = { @Content(mediaType = "application/json",
			// Use the `array` property instead of `schema`
			array = @ArraySchema(schema = @Schema(implementation = Rectangle.class))) })
	@GetMapping
	public Iterable<Rectangle> findAll(
			@RequestParam(required = false) @Parameter(description = "The area we want to search for") final Optional<Long> area,
			@RequestParam(required = false) @Parameter(description = "The minimum length we want to search for") final Optional<Integer> length) {
		// there are two optional query string parameters to filter the rectangle
		// collection
		if (area.isPresent()) {
			return service.findByArea(area.get());
		}
		if (length.isPresent()) {
			return service.findByMinLength(length.get());
		}

		// we assume the default behavior of Spring to transform the Rectangle in a DTO
		// is enough, so we return the Rectangle list here.
		// otherwise we would need to convert each rectangle into a DTO
		//
		// List<RectangleDTO> ret = new ArrayList<>();
		// for (Rectangle r: service.findAll()) {
		// ret.add(rectangleMapper.toDTO(r));
		// }
		// return ret;
		//
		return service.findAll();
	}

	@Operation(summary = "Gets a specific rectangle")
	@GetMapping(value = "/{id}")
	public Rectangle findById(
			@PathVariable("id") @Parameter(description = "The id of the rectangle to find") final int id) {
		return service.findOne(id)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Rectangle " + id + " Not Found"));
	}

	/**
	 * The server determines the id of the resource since the client sends the name,
	 * so POST to the collection.
	 *
	 * @param resource
	 * @return
	 */
	@Operation(summary = "Creates a new rectangle")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Rectangle> create(@RequestBody final CreateRectangleRequest resource) {
		try {
			final var rect = service.create(resource);

			// since the server creates the id, it should return the location of the new
			final var newRectUri = ServletUriComponentsBuilder.fromCurrentRequestUri()
					.pathSegment(rect.getId().toString()).build().toUri();

			return ResponseEntity.created(newRectUri).body(rect);
		} catch (final IllegalArgumentException ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad request", ex);
		}
	}

//	@PutMapping("/{name}")
//	ImageDTO create(String name, ImageDTO dto) {
//		dto.setName(name);
//
//	}

	/**
	 * PUT is used either to fully replace an existing resource or create a new
	 * resource, i.e., UPSERT. However since it is the server responsibility to
	 * assign the id, for this resource PUT will only fully update existing
	 * resources and will return a 400 Bad request otherwise
	 *
	 * @param request
	 * @param id
	 * @param resource
	 * @return
	 * @throws URISyntaxException
	 */
	@Operation(summary = "Fully replaces an existing rectangle.")
	@PutMapping(value = "/{id}")
	public ResponseEntity<Rectangle> update(
			@PathVariable("id") @Parameter(description = "The id of the Rect to replace/create") final int id,
			@RequestBody final EditRectangleRequest resource) {
		try {
			final var rect = service.update(id, resource);
			return ResponseEntity.ok().body(rect);
		} catch (final NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Use POST /rectangle to create new rectangles");
		}
	}

	@Operation(summary = "Partially updates an existing rectangle")
	@PatchMapping(value = "/{id}")
	public ResponseEntity<Rectangle> partialUpdate(
			@PathVariable("id") @Parameter(description = "The id of the rectangle to update") final int id,
			@RequestBody final EditRectangleRequest resource) {
		try {
			final var rect = service.partialUpdate(id, resource);
			return ResponseEntity.ok().body(rect);
		} catch (final NotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Rectangle " + id + " Not Found");
		}
	}

	@Operation(summary = "Deletes an existing rectangle")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Rectangle> delete(
			@PathVariable("id") @Parameter(description = "The id of the rectangle to delete") final int id) {
		final int count = service.deleteById(id);

		return count == 1 ? ResponseEntity.noContent().build() : ResponseEntity.status(404).build();
	}
}
