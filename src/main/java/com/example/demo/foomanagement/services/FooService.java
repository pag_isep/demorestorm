/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.foomanagement.services;

import java.util.Optional;

import com.example.demo.foomanagement.model.Foo;

/**
 *
 * @author pgsousa
 *
 */
public interface FooService {

	/**
	 *
	 * @return
	 */
	Iterable<Foo> findAll();

	/**
	 *
	 * @param id
	 * @return
	 */
	Optional<Foo> findOne(String id);

	/**
	 * Creates a new Foo with the specified id.
	 *
	 * @param id
	 * @param resource
	 * @return
	 */
	Foo create(String id, EditFooRequest resource);

	/**
	 * Updates (fully replaces) an existing Foo.
	 *
	 * @param id
	 * @param resource
	 * @param desiredVersion
	 * @return
	 */
	Foo update(String id, EditFooRequest resource, long desiredVersion);

	/**
	 * Partial updates an existing Foo.
	 *
	 * @param id
	 * @param resource  "patch document"
	 * @param parseLong
	 * @return
	 */
	Foo partialUpdate(String id, EditFooRequest resource, long parseLong);

	/**
	 *
	 * @param id
	 * @param desiredVersion
	 */
	int deleteById(String id, long desiredVersion);

}