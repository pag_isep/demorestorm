/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.foomanagement.services;

import java.util.Optional;

import org.hibernate.StaleObjectStateException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.foomanagement.model.Foo;
import com.example.demo.foomanagement.repositories.FooRepository;

import lombok.RequiredArgsConstructor;

/**
 *
 * @author pgsousa
 *
 */
@Service
@RequiredArgsConstructor
public class FooServiceImpl implements FooService {

	private final FooRepository repository;
	private final EditFooMapper fooEditMapper;

	@Override
	public Iterable<Foo> findAll() {
		return repository.findAll();
	}

	@Override
	public Optional<Foo> findOne(final String id) {
		return repository.findByName(id);
	}

	@Override
	public Foo create(final String id, final EditFooRequest resource) {
		// construct a new object based on data received by the service
		final Foo obj = fooEditMapper.create(id, resource);

		// TODO ensure domain invariants or does the Mapper runs validations?

		return repository.save(obj);
	}

	public Foo upsert(final String id, final EditFooRequest resource, final Long desiredVersion) {
		// first check if the object is already in the persistence store to determine if
		// we are updating or inserting data
		final var maybeFoo = repository.findByName(id);
		if (!maybeFoo.isPresent()) {
			// insert
			return create(id, resource);
		}

		// update
		final Foo foo = maybeFoo.get();

		// validate optimistic locking in memory. it will be validated anyway when
		// performing save
		if (foo.getVersion() != desiredVersion) {
			throw new StaleObjectStateException("Object was already modified by another user", id);
		}

		// apply update
		fooEditMapper.update(resource, foo);
		// foo.updateData(desiredVersion, resource.getDescription(),
		// resource.getLength(), resource.getWidth());

		return repository.save(foo);
	}

	@Override
	public Foo update(final String id, final EditFooRequest resource, final long desiredVersion) {
		// first let's check if the object exists so we don't create a new object with
		// save
		final var foo = repository.findByName(id)
				.orElseThrow(() -> new NotFoundException("Cannot update an object that does not yet exist"));

		// apply update
		foo.updateData(desiredVersion, resource.getDescription(), resource.getLength(), resource.getWidth());

		return repository.save(foo);
	}

	@Override
	@Transactional
	public int deleteById(final String id, final long desiredVersion) {
		return repository.deleteByNameIfMatch(id, desiredVersion);
	}

	@Override
	public Foo partialUpdate(final String id, final EditFooRequest resource, final long desiredVersion) {
		// first let's check if the object exists so we don't create a new object with
		// save
		final var foo = repository.findByName(id)
				.orElseThrow(() -> new NotFoundException("Cannot update an object that does not yet exist"));

		// since we got the object from the database we can check the version in memory
		// and apply the patch
		foo.applyPatch(desiredVersion, resource.getDescription(), resource.getLength(), resource.getWidth());

		// in the meantime some other user might have changed this object on the
		// database, so concurrency control will still be applied when we try to save
		// this updated object
		return repository.save(foo);
	}

}
