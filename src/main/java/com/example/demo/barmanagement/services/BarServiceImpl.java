/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.barmanagement.services;

import java.util.Optional;

import jakarta.validation.ValidationException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.barmanagement.model.Bar;
import com.example.demo.barmanagement.repositories.BarRepository;
import com.example.demo.exceptions.NotFoundException;
import com.example.demo.foomanagement.repositories.FooRepository;

import lombok.RequiredArgsConstructor;

/**
 *
 * @author pgsousa
 *
 */
@Service
@RequiredArgsConstructor
public class BarServiceImpl implements BarService {

	private final BarRepository barRepository;
	private final FooRepository fooRepository;
	private final EditBarMapper fooEditMapper;

	@Override
	public Iterable<Bar> findAll() {
		return barRepository.findAll();
	}

	@Override
	public Optional<Bar> findOne(final Long id) {
		return barRepository.findById(id);
	}

	@Override
	public Bar create(final CreateBarRequest resource) {
		// construct a new object based on data received by the service
		final Bar bar = fooEditMapper.create(resource);

		// TODO ensure domain invariants or does the Mapper runs validations?
		return barRepository.save(bar);
	}

	@Override
	public Bar update(final Long id, final EditBarRequest resource, final long desiredVersion) {
		// first let's check if the object exists so we don't create a new object with
		// save
		final var bar = barRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("Cannot update an object that does not yet exist"));

		// we need to search the related Foo object so we can reference it in the new
		// Bar
		final var foo = fooRepository.findByName(resource.getFoo())
				.orElseThrow(() -> new ValidationException("Select an existing foo"));

		// apply update
		bar.updateData(desiredVersion, resource.getShortNote(), resource.getDescription(), foo);

		return barRepository.save(bar);
	}

	@Override
	@Transactional
	public int deleteById(final Long id, final long desiredVersion) {
		return barRepository.deleteByIdIfMatch(id, desiredVersion);
	}

	@Override
	public Bar partialUpdate(final Long id, final EditBarRequest resource, final long desiredVersion) {
		// first let's check if the object exists so we don't create a new object with
		// save
		final var bar = barRepository.findById(id)
				.orElseThrow(() -> new NotFoundException("Cannot update an object that does not yet exist"));

		// we need to search the related Foo object so we can reference it in the new
		// Bar
		final var foo = fooRepository.findByName(resource.getFoo())
				.orElseThrow(() -> new ValidationException("Select an existing foo"));

		// since we got the object from the database we can check the version in memory
		// and apply the patch
		bar.applyPatch(desiredVersion, resource.getShortNote(), resource.getDescription(), foo);

		// in the meantime some other user might have changed this object on the
		// database, so concurrency control will still be applied when we try to save
		// this updated object
		return barRepository.save(bar);
	}

}
