/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.simple.rectanglemanagement.infrastructure.repositories.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.example.demo.simple.rectanglemanagement.model.Rectangle;
import com.example.demo.simple.rectanglemanagement.repositories.RectangleRepository;

/**
 * An implementation of the repository using an in memory array list
 *
 * @author pgsousa
 *
 */
@Component
@Profile("demo.simple.rectangleRepo.List")
public class ListRectangleRepository implements RectangleRepository {

	private final List<Rectangle> data = new ArrayList<>();

	@Override
	public Optional<Rectangle> findById(final int id) {
		for (final var r : data) {
			if (r.getId() == id) {
				return Optional.of(r);
			}
		}
		return Optional.empty();
	}

	@Override
	public List<Rectangle> findByLengthGreaterThan(final int minLength) {
		final List<Rectangle> result = new ArrayList<>();
		for (final Rectangle r : data) {
			if (r.getLength() >= minLength) {
				result.add(r);
			}
		}
		return result;
	}

	@Override
	public List<Rectangle> findByArea(final long area) {
		final List<Rectangle> result = new ArrayList<>();
		for (final Rectangle r : data) {
			if (r.getArea() == area) {
				result.add(r);
			}
		}
		return result;
	}

	private int indexOfId(final int id) {
		int i = 0;
		for (final var r : data) {
			if (r.getId() == id) {
				return i;
			}
			i++;
		}
		return -1;
	}

	@Override
	public int deleteById(final int id) {
		final int idx = indexOfId(id);
		if (idx != -1) {
			data.remove(idx);
			return 1;
		}
		return 0;
	}

	@Override
	public Iterable<Rectangle> findAll() {
		return data;
	}

	private static AtomicInteger nextID = new AtomicInteger(1);

	@Override
	public Rectangle save(final Rectangle obj) {
		if (!obj.hasId()) {
			obj.setId(nextID.getAndIncrement());
			data.add(obj);
		}
		return obj;
	}
}