/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.foomanagement.api;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.exceptions.NotFoundException;
import com.example.demo.foomanagement.model.Foo;
import com.example.demo.foomanagement.services.EditFooRequest;
import com.example.demo.foomanagement.services.FileStorageService;
import com.example.demo.foomanagement.services.FooService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;

/**
 *
 * @author pgsousa
 *
 */
@Tag(name = "Foos", description = "Endpoints for managing foos")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/foo")
class FooController {

	private static final String IF_MATCH_HEADER = "If-Match";

	private static final Logger logger = LoggerFactory.getLogger(FooController.class);

	private final FooService service;
	private final FileStorageService fileStorageService;
	private final FooViewMapper fooViewMapper;

	@Operation(summary = "Gets all foos")
	@ApiResponse(description = "Success", responseCode = "200", content = { @Content(mediaType = "application/json",
			// Use the `array` property instead of `schema`
			array = @ArraySchema(schema = @Schema(implementation = FooView.class))) })
	@GetMapping
	public Iterable<FooView> findAll() {
		return fooViewMapper.toFooView(service.findAll());
	}

	@Operation(summary = "Gets a specific foo")
	@GetMapping(value = "/{id}")
	public ResponseEntity<FooView> findById(
			@PathVariable("id") @Parameter(description = "The id of the foo to find") final String id) {
		final var foo = service.findOne(id).orElseThrow(() -> new NotFoundException(Foo.class, id));

		return ResponseEntity.ok().eTag(Long.toString(foo.getVersion())).body(fooViewMapper.toFooView(foo));
	}

	private Long getVersionFromIfMatchHeader(final String ifMatchHeader) {
		if (ifMatchHeader.startsWith("\"")) {
			return Long.parseLong(ifMatchHeader.substring(1, ifMatchHeader.length() - 1));
		}
		return Long.parseLong(ifMatchHeader);
	}

	/**
	 * PUT is used either to fully replace an existing resource or create a new
	 * resource, i.e., UPSERT
	 *
	 * @param request
	 * @param id
	 * @param resource
	 * @return
	 * @throws URISyntaxException
	 */
	//
	@Operation(summary = "Fully replaces an existing foo or creates a new one if the specified id does not exist.")
	@PutMapping(value = "/{id}")
	public ResponseEntity<FooView> upsert(final WebRequest request,
			@PathVariable("id") @Parameter(description = "The id of the foo to replace/create") final String id,
			@Valid @RequestBody final EditFooRequest resource) {
		final String ifMatchValue = request.getHeader(IF_MATCH_HEADER);
		if (ifMatchValue == null || ifMatchValue.isEmpty()) {
			// no if-match header was sent, so we are in INSERT mode
			final var foo = service.create(id, resource);
			final var newFooUri = ServletUriComponentsBuilder.fromCurrentRequestUri().pathSegment(foo.getName()).build()
					.toUri();
			return ResponseEntity.created(newFooUri).eTag(Long.toString(foo.getVersion()))
					.body(fooViewMapper.toFooView(foo));
		}
		// if-match header was sent, so we are in UPDATE mode
		final var foo = service.update(id, resource, getVersionFromIfMatchHeader(ifMatchValue));
		return ResponseEntity.ok().eTag(Long.toString(foo.getVersion())).body(fooViewMapper.toFooView(foo));
	}

	@Operation(summary = "Partially updates an existing foo")
	@PatchMapping(value = "/{id}")
	public ResponseEntity<FooView> partialUpdate(final WebRequest request,
			@PathVariable("id") @Parameter(description = "The id of the foo to update") final String id,
			@Valid @RequestBody final EditFooRequest resource) {
		final String ifMatchValue = request.getHeader(IF_MATCH_HEADER);
		if (ifMatchValue == null || ifMatchValue.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"You must issue a conditional PATCH using 'if-match'");
		}

		final var foo = service.partialUpdate(id, resource, getVersionFromIfMatchHeader(ifMatchValue));
		return ResponseEntity.ok().eTag(Long.toString(foo.getVersion())).body(fooViewMapper.toFooView(foo));
	}

	@Operation(summary = "Deletes an existing foo")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<FooView> delete(final WebRequest request,
			@PathVariable("id") @Parameter(description = "The id of the foo to delete") final String id) {
		final String ifMatchValue = request.getHeader(IF_MATCH_HEADER);
		if (ifMatchValue == null || ifMatchValue.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"You must issue a conditional DELETE using 'if-match'");
		}
		final int count = service.deleteById(id, getVersionFromIfMatchHeader(ifMatchValue));

		// TODO check if we can distinguish between a 404 and a 409
		return count == 1 ? ResponseEntity.noContent().build() : ResponseEntity.status(409).build();
	}

	/*
	 * Handling files as subresources
	 */

	/**
	 * Upload a new image.
	 *
	 * <p>
	 * code based on
	 * https://github.com/callicoder/spring-boot-file-upload-download-rest-api-example
	 *
	 * @param request
	 * @param file
	 * @return
	 * @throws URISyntaxException
	 */
	@Operation(summary = "Uploads a photo of a foo")
	@PostMapping("/{fooid}/photo")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<UploadFileResponse> uploadFile(@PathParam("fooid") final String fooid,
			@RequestParam("file") final MultipartFile file) throws URISyntaxException {

		final UploadFileResponse up = doUploadFile(fooid, file);

		return ResponseEntity.created(new URI(up.getFileDownloadUri())).body(up);

	}

	public UploadFileResponse doUploadFile(final String id, final MultipartFile file) {

		final String fileName = fileStorageService.storeFile(id, file);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentRequestUri().pathSegment(fileName)
				.toUriString();
		// since we are reusing this method both for single file upload and multiple
		// file upload and have different urls we need to make sure we always return the
		// right url for the single file download
		fileDownloadUri = fileDownloadUri.replace("/photos/", "/photo/");

		// TODO save info of the file on the database

		return new UploadFileResponse(fileName, fileDownloadUri, file.getContentType(), file.getSize());
	}

	/**
	 * Upload a set of images.
	 *
	 * <p>
	 * code based on
	 * https://github.com/callicoder/spring-boot-file-upload-download-rest-api-example
	 *
	 * @param request
	 * @param file
	 * @return
	 */
	@Operation(summary = "Uploads a set of photos of a foo")
	@PostMapping("/{id}/photos")
	@ResponseStatus(HttpStatus.CREATED)
	public List<UploadFileResponse> uploadMultipleFiles(@PathVariable("id") final String id,
			@RequestParam("files") final MultipartFile[] files) {
		return Arrays.asList(files).stream().map(f -> doUploadFile(id, f)).collect(Collectors.toList());
	}

	/**
	 *
	 * <p>
	 * code based on
	 * https://github.com/callicoder/spring-boot-file-upload-download-rest-api-example
	 *
	 * @param fileName
	 * @param request
	 * @return
	 */
	@Operation(summary = "Downloads a photo of a foo")
	@GetMapping("/{id}/photo/{fileName:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable final String fileName,
			final HttpServletRequest request) {
		// Load file as Resource
		final Resource resource = fileStorageService.loadFileAsResource(fileName);

		// Try to determine file's content type
		String contentType = null;
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		} catch (final IOException ex) {
			logger.info("Could not determine file type.");
		}

		// Fallback to the default content type if type could not be determined
		if (contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
	}
}
