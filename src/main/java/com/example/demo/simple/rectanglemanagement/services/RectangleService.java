/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.simple.rectanglemanagement.services;

import java.util.Optional;

import com.example.demo.simple.rectanglemanagement.model.Rectangle;

/**
 *
 * @author pgsousa
 *
 */
public interface RectangleService {

	/**
	 *
	 * @return
	 */
	Iterable<Rectangle> findAll();

	/**
	 *
	 * @param id
	 * @return
	 */
	Optional<Rectangle> findOne(int id);

	/**
	 * Creates a new rectangle with the specified id.
	 *
	 * @param id
	 * @param resource
	 * @return
	 */
	Rectangle create(CreateRectangleRequest resource);

	/**
	 * Updates (fully replaces) an existing rectangle.
	 *
	 * @param id
	 * @param resource
	 * @return
	 */
	Rectangle update(int id, EditRectangleRequest resource);

	/**
	 * Partial updates an existing rectangle.
	 *
	 * @param id
	 * @param resource "patch document"
	 * @return
	 */
	Rectangle partialUpdate(int id, EditRectangleRequest resource);

	/**
	 *
	 * @param id
	 */
	int deleteById(int id);

	Iterable<Rectangle> findByArea(Long long1);

	Iterable<Rectangle> findByMinLength(Integer integer);

}