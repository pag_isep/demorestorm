/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.bootstrapping;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.example.demo.foomanagement.model.Foo;
import com.example.demo.foomanagement.repositories.FooRepository;

import lombok.RequiredArgsConstructor;

/**
 * Spring will load and execute all components that implement the interface
 * CommandLineRunner on startup, so we will use that as a way to bootstrap some
 * data for testing purposes.
 * <p>
 * In order to enable this bootstraping make sure you activate the spring
 * profile "bootstrap" in application.properties
 *
 * @author pgsou
 *
 */
@Component
@RequiredArgsConstructor
@Profile("bootstrap")
public class FooBootstrapper implements CommandLineRunner {

	private final FooRepository fooRepo;

	@Override
	public void run(final String... args) throws Exception {
		if (fooRepo.findByName("f1").isEmpty()) {
			final Foo f1 = new Foo("f1");
			fooRepo.save(f1);
		}

		if (fooRepo.findByName("f2").isEmpty()) {
			final Foo f2 = new Foo("f2");
			f2.setLength(3);
			fooRepo.save(f2);
		}

		if (fooRepo.findByName("f3").isEmpty()) {
			final Foo f3 = new Foo("f3");
			f3.setWidth(4);
			fooRepo.save(f3);
		}

		if (fooRepo.findByName("f4").isEmpty()) {
			final Foo f4 = new Foo("f4");
			f4.setLength(3);
			f4.setWidth(4);
			fooRepo.save(f4);
		}
	}

}
