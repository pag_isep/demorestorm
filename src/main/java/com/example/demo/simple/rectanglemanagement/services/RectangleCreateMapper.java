package com.example.demo.simple.rectanglemanagement.services;

import org.springframework.stereotype.Component;

import com.example.demo.simple.rectanglemanagement.model.Rectangle;

/**
 * This is to showcase a Manual mapper, preferably we should use a mapping
 * framework like MapStruct
 */
@Component
public class RectangleCreateMapper {

	public Rectangle create(final CreateRectangleRequest resource) {
		try {
			return new Rectangle(resource.getWidth(), resource.getLength(), resource.getDescription());
		} catch (final NullPointerException ex) {
			throw new IllegalArgumentException(ex);
		}
	}
}
