/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.simple.rectanglemanagement.infrastructure.repositories.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.example.demo.simple.rectanglemanagement.model.Rectangle;
import com.example.demo.simple.rectanglemanagement.repositories.RectangleRepository;

/**
 * An implementation of the repository using an in memory hash map
 *
 * @author pgsousa
 *
 */
@Component
@Profile("demo.simple.rectangleRepo.Map")
public class MapRectangleRepository implements RectangleRepository {

	private final Map<Integer, Rectangle> data = new HashMap<>();

	@Override
	public Optional<Rectangle> findById(final int id) {
		return Optional.ofNullable(data.get(id));
	}

	@Override
	public List<Rectangle> findByLengthGreaterThan(final int minLength) {
		final List<Rectangle> result = new ArrayList<>();
		for (final Rectangle r : data.values()) {
			if (r.getLength() >= minLength) {
				result.add(r);
			}
		}
		return result;
	}

	@Override
	public List<Rectangle> findByArea(final long area) {
		final List<Rectangle> result = new ArrayList<>();
		for (final Rectangle r : data.values()) {
			if (r.getArea() == area) {
				result.add(r);
			}
		}
		return result;
	}

	@Override
	public int deleteById(final int id) {
		final var r = data.remove(id);
		return r == null ? 0 : 1;
	}

	@Override
	public Iterable<Rectangle> findAll() {
		return data.values();
	}

	private static AtomicInteger nextID = new AtomicInteger(1);

	@Override
	public Rectangle save(final Rectangle obj) {
		if (!obj.hasId()) {
			obj.setId(nextID.getAndIncrement());
		}
		return data.put(obj.getId(), obj);
	}
}