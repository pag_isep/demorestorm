/*
 * Copyright (c) 2022-2024 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.example.demo.simple.rectanglemanagement.model;

import java.util.Optional;

/**
 *
 * @author pgsousa
 *
 */
public class Rectangle {

	/**
	 * generated identity. in this case we are delegating to the persistence layer
	 * the generation of the identity
	 */
	private Integer id;

	// optional
	private String description;

	private int width;

	private int length;

	public Rectangle(final int width, final int length, final String description) {
		this(width, length);
		setDescription(description);
	}

	public Rectangle(final int width, final int length) {
		setWidth(width);
		setLength(length);
	}

	public Optional<String> getDescription() {
		return Optional.ofNullable(description);
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(final int width) {
		if (width < 0) {
			throw new IllegalArgumentException("Width must be positive");
		}
		this.width = width;
	}

	public int getLength() {
		return length;
	}

	public void setLength(final int length) {
		if (length < 0) {
			throw new IllegalArgumentException("Length must be positive");
		}
		this.length = length;
	}

	public Long getArea() {
		return (long) width * length;
	}

	public void applyPatch(final String description, final Integer length, final Integer width) {
		// apply patch only if the field was sent in the request. we do not allow to
		// change the id attribute
		if (description != null) {
			setDescription(description);
		}
		if (length != null) {
			setLength(length);
		}
		if (width != null) {
			setWidth(width);
		}
	}

	public void updateData(final String description, final int length, final int width) {
		// update data. we do not allow to change the id attribute
		// length and width are mandatory
		setDescription(description);
		setLength(length);
		// if the request didn't contain a value for a field it will be unset on this
		// object.
		setWidth(width);
	}

	public Integer getId() {
		return id;
	}

	public boolean hasId() {
		return id != null;
	}

	/**
	 * PUBLIC BUT NOT PUBLISHED. this method exists only to support the in memory
	 * persistence layer and should not be used in any other way.
	 *
	 * @param id
	 */
	public void setId(final int id) {
		if (hasId()) {
			throw new IllegalStateException();
		}
		this.id = id;
	}
}
